﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Text;  // for class Encoding
using System.IO;
namespace Aerovuelos
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    /// 
 
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();


            //  contenedor_central.SetLeft(grupo_login,0);
            //  contenedor_central.SetTop(grupo_login, 0);
            

           // abc.Margin = new Thickness(margen_derecha, margen_superio, 0, 0);
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)

        {
            var h = ((System.Windows.Controls.Panel)Application.Current.MainWindow.Content).ActualHeight;
            var w = ((System.Windows.Controls.Panel)Application.Current.MainWindow.Content).ActualWidth;
            double ancho = columna_central.ActualWidth;
            double alto = principal.ActualHeight;
            double margen_derecha = (ancho - 458) / 2;
            double margen_superior = (alto - 450) / 2;
           /* ancho1.Text = ancho.ToString();
            alto1.Text = alto.ToString();
            * 

            ancho2.Text = margen_derecha.ToString();
            alto2.Text = margen_superior.ToString();*/
            Console.WriteLine("Hola");
            abc.Margin = new Thickness(margen_derecha, margen_superior, 0, 0);
        
        }
        private void actualizar_tamaño(object sender, RoutedEventArgs e)
        {
            var h = ((System.Windows.Controls.Panel)Application.Current.MainWindow.Content).ActualHeight;
            var w = ((System.Windows.Controls.Panel)Application.Current.MainWindow.Content).ActualWidth;
            double ancho = columna_central.ActualWidth;
            double alto = principal.ActualHeight;
            double margen_derecha = (ancho - 458) / 2;
            double margen_superior = (alto - 450) / 2;
           /* ancho1.Text = ancho.ToString();
            alto1.Text = alto.ToString();

            ancho2.Text = margen_derecha.ToString();
            alto2.Text = margen_superior.ToString(); */
            abc.Margin = new Thickness(margen_derecha, margen_superior, 0, 0);

        }
        public class objetos_json
        {
            public string respuesta { get; set; }
            public string usuario { get; set; }
            public string outh_token { get; set; }
            public List<string> lastFiveSpinNumbers { get; set; }
        }

     
        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void ProgressBar_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
    }

    /* CLASE PASSWORDBOX */

    public class PasswordBoxMonitor : DependencyObject
    {
        public static bool GetIsMonitoring(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsMonitoringProperty);
        }

        public static void SetIsMonitoring(DependencyObject obj, bool value)
        {
            obj.SetValue(IsMonitoringProperty, value);
        }

        public static readonly DependencyProperty IsMonitoringProperty =
            DependencyProperty.RegisterAttached("IsMonitoring", typeof(bool), typeof(PasswordBoxMonitor), new UIPropertyMetadata(false, OnIsMonitoringChanged));



        public static int GetPasswordLength(DependencyObject obj)
        {
            return (int)obj.GetValue(PasswordLengthProperty);
        }

        public static void SetPasswordLength(DependencyObject obj, int value)
        {
            obj.SetValue(PasswordLengthProperty, value);
        }

        public static readonly DependencyProperty PasswordLengthProperty =
            DependencyProperty.RegisterAttached("PasswordLength", typeof(int), typeof(PasswordBoxMonitor), new UIPropertyMetadata(0));

        private static void OnIsMonitoringChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pb = d as PasswordBox;
            if (pb == null)
            {
                return;
            }
            if ((bool)e.NewValue)
            {
                pb.PasswordChanged += PasswordChanged;
            }
            else
            {
                pb.PasswordChanged -= PasswordChanged;
            }
        }

        static void PasswordChanged(object sender, RoutedEventArgs e)
        {
            var pb = sender as PasswordBox;
            if (pb == null)
            {
                return;
            }
            SetPasswordLength(pb, pb.Password.Length);
        }
    } 
    
}


    
